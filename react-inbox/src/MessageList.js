import Message from './Message';
import React from 'react';

class MessageList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    mapFunction = () => this.props.messages.map(
        (messages) =>
            <Message key={messages.id}
                id={messages.id}
                subject={messages.subject}
                read={messages.read}
                starred={messages.starred}
                selected={messages.selected}
                labels={messages.labels}
                markRead={this.props.markRead}
                starAndUnstar={this.props.starAndUnstar}
                selectAndDeselect={this.props.selectAndDeselect}
            />
    );

    render() {
        return (
            <div className="MessageList">
                {this.mapFunction()}
            </div>
        );
    };

}

export default MessageList;