import React from 'react';

class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    setClassName = () => {
        if (this.props.read && this.props.selected) {
            return "message row read selected"
        }
        else if (this.props.read && !this.props.selected) {
            return "message row read"
        }
        else if (!this.props.read && this.props.selected) {
            return "message row unread selected"
        }
        else if (!this.props.read && !this.props.selected) {
            return "message row unread"
        }
    }

    render() {
        return (
            <div className={this.setClassName()}>
                <div className="col-xs-1">
                    <div className="row">
                        <div className="col-xs-2">
                            <input type="checkbox" checked={this.props.selected}
                                onChange={(e) => this.props.selectAndDeselect(this.props.id)} />
                        </div>
                        <div className="col-xs-2">
                            <i className={this.props.starred ? "star fa fa-star" : "star fa fa-star-o"}
                                onClick={() => this.props.starAndUnstar(this.props.id)}>
                            </i>
                        </div>
                    </div>
                </div>
                <div className="col-xs-11">
                    {this.props.labels.map((item) => (
                        <span className="label label-warning" key={item}>
                            {item}
                        </span>))}
                    <a href="#" onClick={() => this.props.markRead(this.props.id)}>
                        {this.props.subject}
                    </a>
                </div>
            </div>
        )
    }
}

export default Message;