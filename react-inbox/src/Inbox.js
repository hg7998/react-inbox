import MessageList from './MessageList';
import Toolbar from './Toolbar';
import React from 'react';

class Inbox extends React.Component {
    constructor(state) {
        super(state);
        this.allSelected = false;
        this.state = {
            messages: []
        }

    }

    getMessages = () => {
        fetch("http://localhost:8082/api/messages")
        .then((response) => response.json()
        .then((response) => this.setState({messages: response})))
    } 

    componentDidMount() {
        this.getMessages();
    }

    applyLabel = (label) => {
        let selected = 0;
        let messages = [...this.state.messages];

        messages.map(({ ...message }, index) => {
            if (message.selected && message.labels.indexOf(label) === -1) {
                selected++;
                message.labels = message.labels.concat([label]);
                message.selected = false;
                messages[index] = message;
            }
        })

        selected === 0 ? alert("Please select a message to use the tool bar")
            : this.setState({ messages });
    }

    deleteLabel = (label) => {
        let selected = 0;
        let messages = [...this.state.messages];

        messages.map(({ ...message }, index) => {
            let labelIndex = message.labels.indexOf(label);

            if (message.selected) {
                selected++;
            }
            if (labelIndex !== -1) {
                message.labels.splice(labelIndex, 1);
                message.selected = false;
                messages[index] = message;
            }
        })

        selected === 0 ? alert("Please select a message to use the tool bar")
            : this.setState({ messages });
    }


    markRead = (id) => {
        let messages = [...this.state.messages];
        let index = messages.findIndex((element) => element.id === id)
        let message = { ...messages[index] }

        message.read = true;
        messages[index] = message;
        this.setState({ messages });
    };

    markReadButton = () => {
        let selected = 0;
        let messages = [...this.state.messages];

        messages.map(({ ...message }, index) => {
            if (message.selected) {
                selected++;
                message.selected = false;
                message.read = true;
                messages[index] = message;
            }
        })

        selected === 0 ? alert("Please select a message to use the tool bar")
            : this.setState({ messages });
    }

    markUnreadButton = () => {
        let selected = 0;
        let messages = [...this.state.messages];

        messages.map(({ ...message }, index) => {
            if (message.selected) {
                selected++;
                message.selected = false;
                message.read = false;
                messages[index] = message;
            }
        })

        selected === 0 ? alert("Please select a message to use the tool bar")
            : this.setState({ messages });
    }

    starAndUnstar = (id) => {
        let messages = [...this.state.messages];
        let index = messages.findIndex((element) => element.id === id)
        let message = { ...messages[index] }

        message.starred ? message.starred = false : message.starred = true;
        messages[index] = message;
        this.setState({ messages });
    }

    selectAndDeselect = (id) => {
        let messages = [...this.state.messages];
        let index = messages.findIndex((element) => element.id === id)
        let message = { ...messages[index] }

        message.selected ? message.selected = false : message.selected = true;
        messages[index] = message;
        this.setState({ messages });
    }

    selectAll = () => {
        let messages = [...this.state.messages];

        messages.map(({ ...message }, index) => {
            message.selected = true;
            messages[index] = message;
        })

        this.allSelected = true;
        this.setState({ messages });
    }

    deSelectAll = () => {
        let messages = [...this.state.messages];

        messages.map(({ ...message }, index) => {
            message.selected = false;
            messages[index] = message;
        })

        this.allSelected = false;
        this.setState({ messages });
    }

    deleteButton = () => {
        let selected = 0;
        let messages = [...this.state.messages];

        for (var i = 0; i < messages.length; i++) {
            let message = { ...messages[i] };
            if (message.selected) {
                selected++;
                messages.splice(i, 1);
                i--;
            }
        }

        selected === 0 ? alert("Please select a message to use the tool bar")
            : this.setState({ messages })
    }


    render() {
        return (
            <>
                <Toolbar
                    messages={this.state.messages}
                    selectAll={this.selectAll}
                    deSelectAll={this.deSelectAll}
                    allSelected={this.allSelected}
                    markReadButton={this.markReadButton}
                    markUnreadButton={this.markUnreadButton}
                    deleteButton={this.deleteButton}
                    applyLabel={this.applyLabel}
                    deleteLabel={this.deleteLabel} />

                <MessageList
                    messages={this.state.messages}
                    markRead={this.markRead}
                    starAndUnstar={this.starAndUnstar}
                    selectAndDeselect={this.selectAndDeselect} />
            </>
        );
    };

}

export default Inbox;