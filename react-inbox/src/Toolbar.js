import React from "react";

class Toolbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    unreadMessages = () => {
        let unread = 0;
        
        this.props.messages.map((message) => { 
            if (!message.read) unread++; 
        })
        return unread;
    }

    selectButtonState = () => {
        let selected = 0;

        this.props.messages.map((message) => {
            if (message.selected) selected++
        })

        if (selected === this.props.messages.length) {
            return "fa fa-check-square-o"
        } else if (selected >= 1) {
            return "fa fa-minus-square-o"
        } else {
            return "fa fa-square-o"
        }
    }

    render() {
        return (
            <div className="row toolbar">
                <div className="col-md-12">
                    <p className="pull-right">
                        <span className="badge badge">{this.unreadMessages()}</span>
                        unread messages
                    </p>

                    <button className="btn btn-default" onClick={() => this.props.allSelected ?
                        this.props.deSelectAll() : this.props.selectAll()}>
                        <i className={this.selectButtonState()}></i>
                    </button>

                    <button className="btn btn-default" onClick={() => this.props.markReadButton()}>
                        Mark As Read
                    </button>

                    <button className="btn btn-default" onClick={() => this.props.markUnreadButton()}>
                        Mark As Unread
                    </button>

                    <select className="form-control label-select" onChange={(e) => this.props.applyLabel(e.target.value)}>
                        <option value="default">Apply label</option>
                        <option value="dev">dev</option>
                        <option value="personal">personal</option>
                        <option value="gschool">gschool</option>
                    </select>

                    <select className="form-control label-select"
                        onChange={(e) => this.props.deleteLabel(e.target.value)}>
                        <option>Remove label</option>
                        <option value="dev">dev</option>
                        <option value="personal">personal</option>
                        <option value="gschool">gschool</option>
                    </select>

                    <button className="btn btn-default" onClick={() => this.props.deleteButton()}>
                        <i className="fa fa-trash-o"></i>
                    </button>
                </div>
            </div>
        )
    }
}

export default Toolbar;